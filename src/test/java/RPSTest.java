import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RPSTest {

    RPS game = new RPS();
    String result;

    @Test
    public void shouldPlay1WinsWhenPlayPaper() {

        result = game.play(TrowEnum.PAPER, TrowEnum.ROCK);
        AssertEqualsAndReturnResult(ResultEnum.PLAYER_1_WINS.getDescricao(), result);
    }

    @Test
    public void shouldPlay1WinsWhenPlayScissors() {

        result = game.play(TrowEnum.SCISSORS, TrowEnum.PAPER);
        AssertEqualsAndReturnResult(ResultEnum.PLAYER_1_WINS.getDescricao(), result);
    }

    @Test
    public void shouldPlay1WinsWhenPlayRock() {

        result = game.play(TrowEnum.ROCK, TrowEnum.SCISSORS);
        AssertEqualsAndReturnResult(ResultEnum.PLAYER_1_WINS.getDescricao(), result);
    }

    @Test
    public void shouldPlay1LoosesWhenPlayPaper() {

        result = game.play(TrowEnum.PAPER, TrowEnum.SCISSORS);
        AssertEqualsAndReturnResult(ResultEnum.PLAYER_2_WINS.getDescricao(), result);
    }

    @Test
    public void shouldPlay1LoosesWhenPlayScissors() {

        result = game.play(TrowEnum.SCISSORS, TrowEnum.ROCK);
        AssertEqualsAndReturnResult(ResultEnum.PLAYER_2_WINS.getDescricao(), result);
    }

    @Test
    public void shouldPlay1LoosesWhenPlayRock() {

        String result = game.play(TrowEnum.ROCK, TrowEnum.PAPER);
        AssertEqualsAndReturnResult(ResultEnum.PLAYER_2_WINS.getDescricao(), result);
    }

    @Test
    public void shouldPlay1TiesWhenPlayPaper() {

        result = game.play(TrowEnum.PAPER, TrowEnum.PAPER);
        AssertEqualsAndReturnResult(ResultEnum.TIE.getDescricao(), result);
    }

    @Test
    public void shouldPlay1TiesWhenPlayScissors() {

        result = game.play(TrowEnum.SCISSORS, TrowEnum.SCISSORS);
        AssertEqualsAndReturnResult(ResultEnum.TIE.getDescricao(), result);
    }

    @Test
    public void shouldPlay1TiesWhenPlayRock() {

        result = game.play(TrowEnum.ROCK, TrowEnum.ROCK);
        AssertEqualsAndReturnResult(ResultEnum.TIE.getDescricao(), result);
    }

    private void AssertEqualsAndReturnResult(String expectedResult, String result) {
        System.out.println(GetMethodName() + ": " + result);
        assertEquals(expectedResult, result);
    }

    private String GetMethodName() {

        String methodName =
                Thread.currentThread().getStackTrace()[3].getMethodName();
        return methodName;
    }
}
