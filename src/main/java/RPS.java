public class RPS {
    public String play(TrowEnum player1Trow, TrowEnum player2Trow) {

        String result = "";
        
        switch (player1Trow) {
            case ROCK:
                switch (player2Trow) {
                    case PAPER:
                        result=ResultEnum.PLAYER_2_WINS.getDescricao();
                        break;
                    case SCISSORS:
                        result=ResultEnum.PLAYER_1_WINS.getDescricao();
                        break;
                    case ROCK:
                        result=ResultEnum.TIE.getDescricao();
                        break;
                }
                break;
            case PAPER:
                switch (player2Trow) {
                    case PAPER:
                        result=ResultEnum.TIE.getDescricao();
                        break;
                    case SCISSORS:
                        result=ResultEnum.PLAYER_2_WINS.getDescricao();
                        break;
                    case ROCK:
                        result=ResultEnum.PLAYER_1_WINS.getDescricao();
                        break;
                }
                break;
            case SCISSORS:
                switch (player2Trow) {
                    case PAPER:
                        result=ResultEnum.PLAYER_1_WINS.getDescricao();
                        break;
                    case SCISSORS:
                        result=ResultEnum.TIE.getDescricao();
                        break;
                    case ROCK:
                        result=ResultEnum.PLAYER_2_WINS.getDescricao();
                        break;
                }
                break;
        }
        return result;
    }
}
