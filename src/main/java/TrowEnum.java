public enum TrowEnum {

    ROCK("rock"),
    PAPER("paper"),
    SCISSORS("scissors");

    private String descricao;

    TrowEnum(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}