public enum ResultEnum {

    PLAYER_1_WINS("Player 1 Wins !"),
    PLAYER_2_WINS("Player 2 Wins !"),
    TIE("Tie !");

    private String descricao;

    ResultEnum(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}